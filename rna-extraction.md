# RNA extraction

## Materials
|Name                             |Manufacturer |Catalog   |Lot #    |
|---------------------------------|-------------|----------|---------|
|Phenol Chloroform Isoamyl Alcohol|Sigma        |P-2069    |127H0627 |
|Sodium Acetate                   |Sigma Aldrich|S2889-2506|SLBN2139V|
|Isoproponal                      |             |          |         |
|Proteinase                       |Bioneer      |KB-0111   |1401D    |
|SDS                              |             |          |         |
|TE                               |BIoneer      |C-9005    |602      |
|Ethanol                          |             |          |         |


## Procedure
1. Add 10 microliters proteinase K(breaks up proteins/cell walls)
    1. Incubate at 60°C for 10 minutes
2. Transfer the homogenate to a fresh polpropylene tube and sequentially add 0.1ml of 2 M sodium acetate(Ph 4.0),1 ml of phenol and 0.2 ml of chloroform-isoamyl alcohol per millilter of Solution D. After addition of each reagent,cap the tube and mix the contents thoroughly by inversion
3. Vortex the homogenate vigouously for 10 seconds. Incubate the tube for 15 minutes on ice to permin complete dissociation of nucleoprotein complexes
4. Centrifuge the tube at 10,000 g(9,000 rpm in a Sorvall SS-34 rotor) for 20 minutes at 4°C , and then transfer the upper aquueous phase containing the extracted RNA o a fresh tube
5. Add an equal volume of isopropanol to the extracted RNA. Mix the solution well and allow the RNA to precipitate for 1 hour or more at -20°C
6. Collect the precipitated RNA by centrifugation at 10,000 g((9,000 rpm in a Sorvall SS-34 rotor) for 30 minutes at 4°C.
7. Carefully decant the ispropanol and dissolve the rNA pellet in 0.3  ml of Solution D for every 1 ml of this solution used in step 1
    1.  Decant the supernatant into a fresh tube. Do not d9scard it until the pellet has been checked
8. Transfer the solution to a microfuge tube, vortex it well and precipitate the RNA with  volume of isopropanol for 1 hour or more at -20°C
 1. If degradation of RNA turns out to be a problem(e.g when isolating RNA from cells or tissues known to contain large amounts of RNase such as macrophages, pancreas and small intestine), repeat steps 7 and 8 once more
9. Collect the precipitated RNA by centrifugation at maximum speed for 10 minutes at 4°C in a microfuge. Wash the pellet twice with 75% ethanol, centrifuge again and remove any remaining ethanol with disposable pipette tip. Store the open tube on the bench for a few minutes to allow the ethanol to evaporate. Do not allow the pellet to dry completely
10. Add 50 - 100 microliters of DEPC treated H20. Store the RNA solution at -70°C
    1. Addition of SDS to 0.5% followed by heating to 65°C may assist dissolution of the pellet
11. Estimate the concentration of the RNA by measuring the absorbance at 260nm of an aliquot of the final preparation as described in appendix 8
12. Dissolve the precipitate in an aqueous buffer and store at -80°C.  Buffers commonly used for this purpose include SDS(0.1% - 0.5%) in TE(PH 7.6) or in DEPC-treated H20 containing 0.1mM EDTA(pH 7.5). the SDS should be removed by chloroform extraction and ehtanol precipitation before enzymatic treatment of the RNA(e.g primer extension, reverse transciption, and in vitro translation)


